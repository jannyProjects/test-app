package com.example.hotovo.hotovo.manager.db

import com.example.hotovo.hotovo.data.db.DataDbOpenHelper
import com.example.hotovo.hotovo.data.db.dao.DatabaseDao
import com.example.hotovo.hotovo.data.db.entity.Feeds
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PlatformManagerImpl(
    private val dataDbOpenHelper: DataDbOpenHelper
) : PlatformManager {

    private var databaseDao: DatabaseDao = dataDbOpenHelper.databaseDao()

    override fun insertFeeds(feeds: Feeds): Flowable<Long> {
        return Flowable.fromCallable {
            databaseDao.insertFeeds(feeds)
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getAllFeeds(): Flowable<List<Feeds>> {
        return databaseDao.getAllFeeds().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }
}