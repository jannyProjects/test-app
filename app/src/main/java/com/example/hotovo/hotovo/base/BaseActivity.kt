package com.example.hotovo.hotovo.base

import androidx.appcompat.app.AppCompatActivity


abstract class BaseActivity<in T : BaseViewModel<*>> : AppCompatActivity() {

    protected abstract fun subscribeViewModel(viewModel: T)

}