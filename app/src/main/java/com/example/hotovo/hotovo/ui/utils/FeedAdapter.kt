package com.example.hotovo.hotovo.ui.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.hotovo.hotovo.R
import com.example.hotovo.hotovo.data.db.entity.Feeds
import kotlinx.android.synthetic.main.feed_layout.view.*


class FeedAdapter(val items: ArrayList<Feeds>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.feed_layout, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = "${items[position].title} \n ${items[position].pubDate}"

        if (items[position].imgUrl != null) {
            Glide.with(context)
                .load(items[position].imgUrl)
                .into(holder.image)
        }
    }
}


class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val image = view.image
    val title = view.title
}
