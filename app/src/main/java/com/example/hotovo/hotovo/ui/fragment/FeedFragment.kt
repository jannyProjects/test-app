package com.example.hotovo.hotovo.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.hotovo.hotovo.R
import com.example.hotovo.hotovo.base.BaseFragment
import com.example.hotovo.hotovo.ui.utils.FeedAdapter
import kotlinx.android.synthetic.main.frg_feed.*
import org.koin.android.viewmodel.ext.android.viewModel

class FeedFragment : BaseFragment<FeedFragmentModel>() {

    companion object {
        fun newInstance(): FeedFragment {
            return FeedFragment()
        }
    }

    private val model: FeedFragmentModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.frg_feed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeViewModel(model)

        getFeeds.setOnClickListener {
            model.getFeed()
        }
        rv_feed_list.layoutManager = LinearLayoutManager(context)
    }

    override fun onResume() {
        super.onResume()
        model.onResume()
    }

    override fun subscribeViewModel(viewModel: FeedFragmentModel) {
        viewModel.getViewModelState().observe(this, Observer { state ->
            if (state != null) {
                when(state) {
                    is FeedFragmentState.Loading -> {
                        getFeeds.visibility = View.INVISIBLE
                        rv_feed_list.visibility = View.INVISIBLE
                        progress.visibility = View.VISIBLE
                        progress.text = "LOADING..."
                    }
                    is FeedFragmentState.Error -> {
                        getFeeds.visibility = View.VISIBLE
                        progress.visibility = View.VISIBLE
                        progress.text = "ERROR"
                    }
                    is FeedFragmentState.Loaded -> {
                        progress.visibility = View.INVISIBLE
                        getFeeds.visibility = View.VISIBLE
                        rv_feed_list.visibility = View.VISIBLE
                        rv_feed_list.adapter = FeedAdapter(state.feeds, context!!)
                    }
                }
            }
        })
    }
}