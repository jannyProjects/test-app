package com.example.hotovo.hotovo.ui.fragment

import com.example.hotovo.hotovo.base.BaseViewModel
import com.example.hotovo.hotovo.data.db.entity.Feeds
import com.example.hotovo.hotovo.manager.db.PlatformManager
import com.example.hotovo.hotovo.manager.rest.RestManger
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable

class FeedFragmentModel(
    private val restManager: RestManger,
    private val platformManager: PlatformManager
) : BaseViewModel<FeedFragmentState>() {

    private val disposables = CompositeDisposable()
    private var feeds = ArrayList<Feeds>()

    override fun updateView(state: FeedFragmentState) {
        this.state.value = state
    }

    fun onResume() {
        updateView(FeedFragmentState.Loaded(feeds))
    }

    fun getFeed() {
        feeds.clear()
        updateView(FeedFragmentState.Loading())
        val disp =
            restManager.getAllFeeds()
                .flatMap {
                    for (i in it.feed.item) {
                        platformManager.insertFeeds(
                            Feeds(
                                title = i.article.title,
                                pubDate = i.article.pubDate,
                                imgUrl = i.article.image?.url,
                                content = i.article.content
                            )
                        ).subscribe()
                    }
                    Flowable.just(true)
                }
                .flatMap {
                    if (it) {
                        platformManager.getAllFeeds()
                    } else {
                        Flowable.just(feeds)
                    }
                }
                .subscribe {
                    feeds.addAll(it)
                    updateView(FeedFragmentState.Loaded(it as ArrayList<Feeds>))
                }
        disposables.add(disp)
    }

    fun getFeeds(): ArrayList<Feeds> {
        return feeds
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}

