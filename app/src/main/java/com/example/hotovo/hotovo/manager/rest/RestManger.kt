package com.example.hotovo.hotovo.manager.rest

import com.example.hotovo.hotovo.data.rest.Feeds
import io.reactivex.Flowable
import io.reactivex.Single

interface RestManger {

    fun getAllFeeds(): Flowable<Feeds>
}