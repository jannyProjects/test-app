package com.example.hotovo.hotovo.base

import androidx.fragment.app.Fragment


abstract class BaseFragment<in T : BaseViewModel<*>> : Fragment() {

    protected abstract fun subscribeViewModel(viewModel: T)

}
