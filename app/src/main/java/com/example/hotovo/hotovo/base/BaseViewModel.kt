package com.example.hotovo.hotovo.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


abstract class BaseViewModel<T : BaseState> : ViewModel() {

    protected var state: MutableLiveData<T> = MutableLiveData()

    fun getViewModelState(): LiveData<T> {
        return state
    }

    abstract fun updateView(state: T)

}
