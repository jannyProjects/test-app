package com.example.hotovo.hotovo.ui.activity

import android.os.Bundle
import androidx.lifecycle.Observer
import com.example.hotovo.hotovo.R
import com.example.hotovo.hotovo.base.BaseActivity
import com.example.hotovo.hotovo.ui.fragment.FeedFragment
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity<MainActivityModel>() {

    private val model: MainActivityModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_main)
        subscribeViewModel(model)

        val fragment = FeedFragment()
        supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer, fragment).commit()
    }

    override fun subscribeViewModel(viewModel: MainActivityModel) {
        viewModel.getViewModelState().observe(this, Observer {

        })
    }
}