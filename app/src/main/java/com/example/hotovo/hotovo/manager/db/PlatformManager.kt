package com.example.hotovo.hotovo.manager.db

import com.example.hotovo.hotovo.data.db.entity.Feeds
import io.reactivex.Flowable
import io.reactivex.Single

interface PlatformManager {

    fun insertFeeds(feeds : Feeds): Flowable<Long>

    fun getAllFeeds(): Flowable<List<Feeds>>
}