package com.example.hotovo.hotovo.data.rest

import com.example.hotovo.hotovo.common.URL_FEED
import io.reactivex.Flowable
import io.reactivex.Single
import retrofit2.http.GET

interface RestService {

    @GET(URL_FEED)
    fun getFeeds(): Flowable<Feeds>
}