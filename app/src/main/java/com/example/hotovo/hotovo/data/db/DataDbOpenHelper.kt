package com.example.hotovo.hotovo.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.hotovo.hotovo.data.db.dao.DatabaseDao
import com.example.hotovo.hotovo.data.db.entity.Feeds

@Database(entities = [Feeds::class], version = 1, exportSchema = false)
abstract class DataDbOpenHelper : RoomDatabase() {

    abstract fun databaseDao(): DatabaseDao

    companion object {
        @Volatile
        private var INSTANCE: DataDbOpenHelper? = null

        fun getInstance(context: Context): DataDbOpenHelper {
            if (INSTANCE == null) {
                synchronized(DataDbOpenHelper::class) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            DataDbOpenHelper::class.java, "platform.db"
                        ).build()
                    }
                }
            }
            return INSTANCE!!
        }
    }

}