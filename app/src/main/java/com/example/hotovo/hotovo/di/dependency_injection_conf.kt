package com.example.hotovo.hotovo.di

import android.content.Context
import com.example.hotovo.hotovo.common.URL_BASE
import com.example.hotovo.hotovo.data.db.DataDbOpenHelper
import com.example.hotovo.hotovo.data.rest.RestService
import com.example.hotovo.hotovo.manager.db.PlatformManager
import com.example.hotovo.hotovo.manager.db.PlatformManagerImpl
import com.example.hotovo.hotovo.manager.rest.RestManger
import com.example.hotovo.hotovo.manager.rest.RestMangerImpl
import com.example.hotovo.hotovo.ui.activity.MainActivityModel
import com.example.hotovo.hotovo.ui.fragment.FeedFragmentModel
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

val applicationModule = module {

    single { createWebService<RestService>(get(), get(), URL_BASE) }
    single { createOkHttpClient(get()) }
    single { createMoshi() }

    single { RestMangerImpl(get()) as RestManger }
    single { PlatformManagerImpl(get()) as PlatformManager }
    single { DataDbOpenHelper.getInstance(get()) }

    viewModel { MainActivityModel() }
    viewModel { FeedFragmentModel(get(), get()) }

}

fun createMoshi(): Moshi {
    return Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()
}

fun createOkHttpClient(context: Context): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    return OkHttpClient.Builder()
        .connectTimeout(60L, TimeUnit.SECONDS)
        .readTimeout(60L, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor).build()
}

inline fun <reified T> createWebService(okHttpClient: OkHttpClient, moshi: Moshi, url: String): T {
    val retrofit = Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
    return retrofit.create(T::class.java)
}
