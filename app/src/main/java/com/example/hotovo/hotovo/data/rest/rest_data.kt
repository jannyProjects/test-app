package com.example.hotovo.hotovo.data.rest

import com.squareup.moshi.Json

data class Feeds(
    @Json(name = "description") val description: String,
    @Json(name = "feed") val feed: Feed
)

data class Feed(
    @Json(name = "title") val title: String,
    @Json(name = "language") val language: String,
    @Json(name = "lastBuildDate") val lastBuildDate: String,
    @Json(name = "item") val item: List<Item>
)

data class Item (
    @Json(name = "article") val article: Article
)

data class Article(
    @Json(name = "id") val id: String,
    @Json(name = "site") val site: String,
    @Json(name = "zone") val zone: String,
    @Json(name = "pubDate") val pubDate: String,
    @Json(name = "lastUpdated") val lastUpdated: String,
    @Json(name = "url") val url: String,
    @Json(name = "twitter") val twitter: String,
    @Json(name = "title") val title: String,
    @Json(name = "introduction") val introduction: String?,
    @Json(name = "content") val content: String?,
    @Json(name = "image") val image: Image?
//    @Json(name = "authors") val authors: List<Authors>?
)

data class Image(
    @Json(name = "id") val id: String,
    @Json(name = "url") val url: String,
    @Json(name = "imageTitle") val imageTitle: String
)

data class Authors(
    @Json(name = "id") val id: String,
    @Json(name = "name") val name: Name,
    @Json(name = "email") val email: String?
)

data class Name(
    @Json(name = "first") val first: String,
    @Json(name = "last") val last: String
)

//data class RelatedStories(
//
//)