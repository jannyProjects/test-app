package com.example.hotovo.hotovo.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.hotovo.hotovo.data.db.entity.Feeds
import io.reactivex.Flowable

@Dao
interface DatabaseDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFeeds(feeds: Feeds): Long

    @Query("select * from feeds")
    fun getAllFeeds(): Flowable<List<Feeds>>
}