package com.example.hotovo.hotovo.ui.activity

import com.example.hotovo.hotovo.base.BaseViewModel


class MainActivityModel : BaseViewModel<MainActivityState>() {

    override fun updateView(state: MainActivityState) {
        this.state.value = state
    }
}