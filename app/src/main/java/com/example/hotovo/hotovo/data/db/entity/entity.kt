package com.example.hotovo.hotovo.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "feeds")
data class Feeds(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long? = null,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "pub_date") val pubDate: String,
    @ColumnInfo(name = "img_url") val imgUrl: String? = null,
    @ColumnInfo(name = "content") val content: String? = null
)