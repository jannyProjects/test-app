package com.example.hotovo.hotovo.manager.rest

import com.example.hotovo.hotovo.data.rest.Feeds
import com.example.hotovo.hotovo.data.rest.RestService
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RestMangerImpl(
    private val restService: RestService
) : RestManger {


    override fun getAllFeeds(): Flowable<Feeds> {
        return restService.getFeeds()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }


}