package com.example.hotovo.hotovo.ui.fragment

import com.example.hotovo.hotovo.base.BaseState
import com.example.hotovo.hotovo.data.db.entity.Feeds

sealed class FeedFragmentState: BaseState {

    class Loading() : FeedFragmentState()
    class Error() : FeedFragmentState()
    class Loaded(val feeds: ArrayList<Feeds>) : FeedFragmentState()
}