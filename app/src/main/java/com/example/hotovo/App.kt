package com.example.hotovo

import android.app.Application
import com.example.hotovo.hotovo.BuildConfig
import com.example.hotovo.hotovo.di.applicationModule
import org.koin.android.ext.android.startKoin
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(applicationModule))
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}